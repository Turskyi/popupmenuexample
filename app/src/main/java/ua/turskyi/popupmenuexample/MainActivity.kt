package ua.turskyi.popupmenuexample

import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.MenuItem
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        menuItem.setOnClickListener {

            val popup: PopupMenu

            val wrapper = ContextThemeWrapper(menuItem.context, R.style.popupMenu)
            popup = PopupMenu(wrapper, menuItem)

            popup.setOnMenuItemClickListener { item: MenuItem? ->
                when (item?.itemId) {
                    R.id.delete -> {
                        Toast.makeText(menuItem.context, item.title, Toast.LENGTH_SHORT)
                            .show()
                    }
                    R.id.minBan -> {
                        Toast.makeText(menuItem.context, item.title, Toast.LENGTH_SHORT)
                            .show()
                    }
                    R.id.maxBan -> {
                        Toast.makeText(menuItem.context, item.title, Toast.LENGTH_SHORT)
                            .show()
                    }
                }
                true
            }
            popup.inflate(R.menu.menu_popup)
            popup.show()
        }
    }
}